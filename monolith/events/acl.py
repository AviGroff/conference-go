import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_location_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    url = f"https://api.pexels.com/v1/search?query={city}+{state}"
    resp = requests.get(url, headers=headers)

    return resp.json()["photos"][0]["url"]


print(get_location_photo("austin", "tx"))


def get_weather(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {"q": f"{city},{state},US", "appid": OPEN_WEATHER_API_KEY}
    response = requests.get(url, params=params)
    content = response.json()

    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    params = {
        "lat": latitude,
        "lon": longitude,
        "units": "imperial",
        "appid": OPEN_WEATHER_API_KEY,
    }

    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    json_resp = response.json()

    try:
        temp = json_resp["main"]["temp"]
        desc = json_resp["weather"][0]["description"]

        return {"temp": temp, "description": desc}
    except (KeyError, IndexError):
        return None
